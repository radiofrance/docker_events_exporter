// Copyright 2019 Radio France
// Licensed under the CeCILL-B, Version 1;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// https://cecill.info/licences/Licence_CeCILL-B_V1-en.txt
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/common/log"
)

type dockerEventsCollector struct {
	filters    []string
	attributes []string
}

func (collector dockerEventsCollector) Run() error {
	client, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		return err
	}

	labels := []string{"event", "type"}
	for _, attribute := range collector.attributes {
		labels = append(labels, strings.ReplaceAll(attribute, ".", "_"))
	}

	dockerEventsCounter := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Help:      "Number of Docker events that have occurred",
			Namespace: "docker",
			Name:      "events",
		},
		labels,
	)

	var rxFilters []*regexp.Regexp
	for _, filter := range collector.filters {
		rx, err := regexp.Compile(filter)
		if err != nil {
			return fmt.Errorf("invalid event filter: %w", err)
		}
		rxFilters = append(rxFilters, rx)
	}

	go collector.run(client, rxFilters, dockerEventsCounter)
	return nil
}

func (collector dockerEventsCollector) run(client *client.Client, filters []*regexp.Regexp, metric *prometheus.CounterVec) {
	log.Info("Start Docker events collector")
	events, errors := client.Events(context.Background(), types.EventsOptions{})

watcher:
	for {
		select {
		case event := <-events:
			for _, filter := range filters {
				if filter.MatchString(event.Status) {
					log.Debugf("Event '%s' matches filter '%s'... event skipped", event.Status, filter)
					continue watcher
				}
			}

			labels := prometheus.Labels{"event": event.Status, "type": event.Type}
			for _, attr := range collector.attributes {
				// NOTE: blank identifier is used to avoid panic if the attribute does not exist.
				// nolint:gosimple
				labels[strings.ReplaceAll(attr, ".", "_")], _ = event.Actor.Attributes[attr]
			}
			metric.With(labels).Inc()

		case err, open := <-errors:
			if !open {
				log.Fatal("Errors channel closed... force shutdown collector")
			} else {
				log.Error(err)
			}
		}
	}
}
