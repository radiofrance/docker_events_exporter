# Docker Events Exporter

Export Docker Events to Prometheus.

To run it:

```bash
make
./docker_events_exporter [flags]
```

## Exported Metrics

| Metric | Meaning | Labels |
| ------ | ------- | ------ |
| docker_events | How many docker events have occured in the cluster | event, type, custom_labels | 

> Default custom labels are `io.kubernetes.pod.name` and `io.kubernetes.pod.namespace`

### Flags

```bash
./docker_events_exporter --help
```

* __`docker.event.ignore`:__ Exclude metrics about specific events _(regexp allowed)_. Example: `^exec_.*` to ignore all event starting with 'exec_'.
* __`docker.event.attr`:__ Include Docker event attributes to the metric labels.
* __`log.format`:__ Set the log target and format. Example: `logger:syslog?appname=bob&local=7`
    or `logger:stdout?json=true`
* __`log.level`:__ Logging level. `info` by default.
* __`version`:__ Show application version.
* __`web.listen-address`:__ Address to listen on for web interface and telemetry.
* __`web.telemetry-path`:__ Path under which to expose metrics.

## Using Docker

Soon, you can deploy this exporter using the rf/docker-events-exporter Docker image.

For example:

```bash
docker pull rf/docker-events-exporter

docker run -d -p 9610:9610 rf/docker-events-exporter --docker.event.ignore="^exec_.*" --docker.event.ignore="die"
```
