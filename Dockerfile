FROM quay.io/prometheus/busybox:latest
LABEL maintainer="Radio France - Wanda Team <noreply>"

COPY docker_events_exporter  /bin/docker_events_exporter

EXPOSE      9610
ENTRYPOINT  [ "/bin/docker_events_exporter" ]