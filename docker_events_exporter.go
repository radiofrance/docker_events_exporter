// Copyright 2019 Radio France
// Licensed under the CeCILL-B, Version 1;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// https://cecill.info/licences/Licence_CeCILL-B_V1-en.txt
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"net/http"
	_ "net/http/pprof"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"github.com/prometheus/common/version"
	"gopkg.in/alecthomas/kingpin.v2"
)

const (
	exporterName = "docker_events_exporter"
)

func main() {
	var (
		listenAddress = kingpin.Flag(
			"web.listen-address",
			"Address to listen on for web interface and telemetry.",
		).Default(":9610").String()

		metricsPath = kingpin.Flag(
			"web.telemetry-path",
			"Path under which to expose metrics.",
		).Default("/metrics").String()

		filters = kingpin.Flag(
			"docker.event.ignore",
			"Exclude metrics about specific events (regexp allowed)",
		).Strings()

		attrs = kingpin.Flag(
			"docker.event.attrs",
			"Include Docker event attributes to the metric labels",
		).Default("io.kubernetes.pod.name", "io.kubernetes.pod.namespace").Strings()
	)

	log.AddFlags(kingpin.CommandLine)
	kingpin.Version(version.Print(exporterName))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	prometheus.MustRegister(version.NewCollector(exporterName))
	log.Infoln("Starting "+exporterName, version.Info())
	log.Infoln("Build context", version.BuildContext())

	collector := &dockerEventsCollector{filters: *filters, attributes: *attrs}
	if err := collector.Run(); err != nil {
		log.Fatal(err)
	}

	if *metricsPath != "/" {
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			_, _ = w.Write([]byte(`<html>
			<head><title>Docker Events Exporter</title></head>
			<body>
			<h1>Docker Events Exporter</h1>
			<p><a href="` + *metricsPath + `">Metrics</a></p>
			</body>
			</html>`))
		})
	}
	http.Handle(*metricsPath, promhttp.Handler())

	log.Infoln("Listening on", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
