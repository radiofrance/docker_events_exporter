module gitlab.com/radiofrance/docker_event_exporter

go 1.16

require (
	github.com/containerd/containerd v1.5.0 // indirect
	github.com/docker/docker v20.10.6+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/moby/term v0.0.0-20201216013528-df9cb8a40635 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.24.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
